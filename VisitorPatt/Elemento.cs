﻿namespace VisitorPatt
{
    //Codigo para representar  a los empleados del restaurante
    abstract class Elemento
    {
        public abstract void Accept(IVisitor visitor);
    }
    class Employee : Elemento
    {
        public string Name { get; set; }
        public double AnnualSalary { get; set; }
        public int PaidTimeOffDays { get; set; }

        public Employee(string name, double annualSalary, int paidTimeOffDays)
        {
            Name = name;
            AnnualSalary = annualSalary;
            PaidTimeOffDays = paidTimeOffDays;
        }


        //Se implementa un visitante que visitara los registros de los empleados, y pagara tiempo libre.
        public override void Accept(IVisitor visitor)
        {
            visitor.Visit(this);

        }
    }
}
