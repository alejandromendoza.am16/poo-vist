﻿namespace VisitorPatt
{
    //Se crea la interfaz visitor (visitante)
    interface IVisitor
    {
        void Visit(Elemento elemento);
    }
}
