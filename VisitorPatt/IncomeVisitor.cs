﻿using System;
using VisitorPatt;

namespace VisitorPatt
{
    class IncomeVisitor : IVisitor
    {
        //Ahora necesitamos a nuestros participantes de ConcreteVisitor, uno para cada detalle sobre los registros de empleados que queremos cambiar.

        public void Visit(Elemento elemento)
        {
            Employee employee = elemento as Employee;

            // Codigo para aumentar el salario en 10% a los empleados.
            employee.AnnualSalary *= 1.10;
            Console.WriteLine("{0} {1}'s nuevos ingresos: {2:C}", employee.GetType().Name, employee.Name, employee.AnnualSalary);
        }
    }
}

/// <summary>
/// A Concrete Visitor class
/// </summary>
class PaidTimeOffVisitor : IVisitor
{
    public void Visit(Elemento elemento)
    {
        Employee employee = elemento as Employee;

        //Codigo donde se les paga a los empleados por los dias libres que son 3
        employee.PaidTimeOffDays += 3;
        Console.WriteLine("{0} {1}'s Dias de vacaciones nuevos: {2}", employee.GetType().Name, employee.Name, employee.PaidTimeOffDays);
    }
}

